package com.stargate.common.errorHandler;



import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class DatabaseConverter implements Callable{

	DatabaseDescriptionHandler databaseDescriptionHandler = new DatabaseDescriptionHandler();
	DatabaseErrorCodeHandler databaseErrorCodeHandler = new DatabaseErrorCodeHandler();
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
	MuleMessage message=eventContext.getMessage();	
    CopyOnWriteArrayList copylist =  (CopyOnWriteArrayList) eventContext.getMessage().getPayload();
		
	List<String> databaseDescription=	databaseDescriptionHandler.description(copylist);
	List<String> databaseError=	databaseErrorCodeHandler.errorCode(copylist);	
		
	message.setProperty("DatabaseSessionVariable", databaseDescription, PropertyScope.SESSION);
	message.setProperty("ErrorSessionVariable", databaseError, PropertyScope.SESSION);
		
		
		return databaseError;
	}

}
