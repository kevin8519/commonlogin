package com.stargate.common.errorHandler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DatabaseErrorCodeHandler {
	Counter counter= new Counter();

	public List<String> errorCode(CopyOnWriteArrayList copylist) {
		// to extract the error from the database output payload
		String s = copylist.toString();

		List<String> list = new ArrayList<String>();

		String[] spliter = s.split(",");
		String pattern = "(?<=HTTP_STATUS_CODE=).*";
		for (String string : spliter) {
			if (string.contains("HTTP_STATUS_CODE")) {
				
				String match = counter.stringMatcher(string, pattern);
				list.add(match.replace("{]", ""));
			}

		}
		
		return list;

	}
}
