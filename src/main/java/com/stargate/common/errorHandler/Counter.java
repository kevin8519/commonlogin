package com.stargate.common.errorHandler;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Counter {
	//Count the number of 'error:' appered in the string
	public int counterFunction(String line) {
		int count = 0;

		Pattern p = Pattern.compile("error:");
		Matcher m = p.matcher(line);
		while (m.find()) {
			count++;
		}
		return count;
	}
	// function to find a pettern in a given string
	public String stringMatcher(String line, String regex) {

		Pattern pattern = Pattern.compile(regex);

		// Now create matcher object.
		Matcher m1 = pattern.matcher(line);
		if (m1.find()) {

			m1.group();
		}
		return m1.group();
	}

}
