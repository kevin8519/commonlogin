package com.stargate.common.errorHandler;

import java.util.ArrayList;
import java.util.List;



public class FieldHandler {
	
	Counter counter= new Counter();
	
	public List<String> fieldChecker(String line) {
		
		// to extract the field name from the given payload
		
		int count = counter.counterFunction(line);
		if (count > 0) {
			return fieldRegex(line, count);
		} else {
			List<String> list = new ArrayList<String>();
			if (line.contains(
					com.stargate.common.errorHandler.ErrorConstants.unknownHeader)) {
				list.add("Unknown Header");
			}
			
			else if (line.contains("Timeout exceeded")) {

				list.add("Mulesoft");
			}

			else if (line.contains("Method Not Supported")) {

				list.add("Method Not Supported");
			}
			else if(line.contains(com.stargate.common.errorHandler.ErrorConstants.jsonParserError)){
				list.add("Invalid Json format");
			}
			
			else if(line.contains(com.stargate.common.errorHandler.ErrorConstants.jsonParserError1)){
				list.add("Invalid Json format");
			}
			
			return list;
		}
	}

	

	public List<String> fieldRegex(String line, int count) {
		
		int i = 1;
		String match=null;
		List<String> list = null;
		String[] word = line.split("error:\\B");

		list = new ArrayList<String>();
		String pattern1 = "(?<=:\"/request-headers/).*";
		String pattern2 = "(?<=:\"/request-body/).*";
		String pattern3="(?<=:\\s\\{\"pointer\":\").*";
		for (i = 1; i <= count; i++) {
			
			if(word[i].contains("request-headers")){

			
			 match = counter.stringMatcher(word[i], pattern1);
			
			}
			
			else if ((word[i].contains("request-body"))&& (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.unknownBody))){
				match = counter.stringMatcher(word[i], pattern3);
			}
			else if (word[i].contains("request-body")){
				match = counter.stringMatcher(word[i], pattern2);
			}
			list.add(match.replaceAll("[^a-zA-Z-_]", ""));

		}
		
		return list;
	}

	

}