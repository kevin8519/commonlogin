package com.stargate.common.errorHandler;

import java.util.List;

import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class LoggingErrorMain implements Callable  {
	
	FieldHandler fieldHandler = new FieldHandler();
	ValueHandler valueHandler = new ValueHandler();
	ErrorManager errorManager = new ErrorManager();
	ActualLenghtAndCharacterFinder actualLenghtAndCharacterFinder = new ActualLenghtAndCharacterFinder();
	

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		
		MuleMessage message=eventContext.getMessage();
	String line=eventContext.getMessage().getPayloadAsString();
	List<String> fieldValue=	fieldHandler.fieldChecker(line);
	List<String> value=	valueHandler.valueChecker(line);
	List<String> queryValue=	errorManager.splliiter(line);
	List<String> actualValue=	actualLenghtAndCharacterFinder.actualvalueChecker(line);
	
	
	message.setProperty("ValueSessionVariable", value, PropertyScope.SESSION);
	message.setProperty("actualLength", actualValue, PropertyScope.SESSION);
	message.setProperty("QuerySessionVariable", queryValue, PropertyScope.SESSION);
	message.setProperty("FieldSessionVariable", fieldValue, PropertyScope.SESSION);
		return queryValue;
	}

}
