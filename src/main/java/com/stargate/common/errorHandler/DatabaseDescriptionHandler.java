package com.stargate.common.errorHandler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class DatabaseDescriptionHandler {
	Counter counter= new Counter();

	public List<String> description(CopyOnWriteArrayList copylist) {
// to extract the description from the database output payload
		String s = copylist.toString();

		List<String> list = new ArrayList<String>();

		String[] spliter = s.split(",");
		String pattern = "(?<=DESCRIPTION=).*";
		for (String string : spliter) {
			if (string.contains("DESCRIPTION")) {
				
				String match = counter.stringMatcher(string, pattern);
				list.add(match.replaceAll("[^a-zA-Z-_]", " "));

			}

		}
		
		return list;

	}
}
