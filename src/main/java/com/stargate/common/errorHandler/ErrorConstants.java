package com.stargate.common.errorHandler;

public class ErrorConstants {
	
	public static final String Authorization="/request-headers/authorization";
	public static final String Content_Type="/request-headers/content-type";
	public static final String gateway_company_id="/request-headers/gateway-company-id";
	
	public static final String request_id="/request-headers/request-id";
	
	public static final String payment_method="/request-body/payment_method";
	public static final String payment_amount="/request-body/payment_amount";
	public static final String payment_date="/request-body/payment_date";
	public static final String payment_id="/request-body/payment_id";
	public static final String debit_credit_indicator="/request-body/debit_credit_indicator";
	public static final String payment_format="/request-body/payment_format";
	public static final String payment_description="/request-body/payment_description";
	public static final String payer_name="/request-body/payer/name";
	public static final String payer_bank_information_bank_id="/request-body/payer/bank_information/bank_id";
	public static final String payer_bank_information_bank_id_type="/request-body/payer/bank_information/bank_id_type";
	public static final String payer_bank_information_bank_account_number="/request-body/payer/bank_information/bank_account_number";
	public static final String payer_bank_information_bank_account_type="/request-body/payer/bank_information/bank_account_type";
	public static final String payee_name="/request-body/payee/name";
	public static final String payee_bank_information_bank_id="/request-body/payee/bank_information/bank_id";
	public static final String payee_bank_information_bank_id_type="/request-body/payee/bank_information/bank_id_type";
	public static final String payee_bank_information_bank_account_number="/request-body/payee/bank_information/bank_account_number";
	public static final String payee_bank_information_bank_account_type="/request-body/payee/bank_information/bank_account_type";
	public static final String shortlen="too short";
	public static final String longlen="too long";
	public static final String maxLength="maxLength";
	public static final String minLength="minLength";
	public static final String maximum="maximum";
	public static final String minimum="minimum";
	public static final String allowed_primitive_type="does not match any allowed primitive type";
	public static final String unknownHeader="Invalid Header Found";
	public static final String unknownBody="object has too many properties";
	public static final String jsonParserError="was expecting comma to separate OBJECT entries";
	public static final String jsonParserError1="was expecting double-quote to start field name";
}
