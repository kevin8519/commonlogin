package com.stargate.common.errorHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class ValueHandler   {

	Counter counter = new Counter();

	public List<String> valueChecker(String line) {
// to extract the field value from the given payload
		int count = 0;
		count = counter.counterFunction(line);

		if (count > 0) {
			return valurRegex(line, count);

		} else {
			List<String> list = new ArrayList<String>();

			if (line.contains(
					com.stargate.common.errorHandler.ErrorConstants.unknownHeader)) {
				list.add(null);
			}
			else if(line.contains(com.stargate.common.errorHandler.ErrorConstants.jsonParserError)){
				list.add(null);
			}
			else if(line.contains(com.stargate.common.errorHandler.ErrorConstants.jsonParserError1)){
				list.add(null);
			}

			return list;
		}
	}

	public List<String> valurRegex(String line, int count) {
		int i = 1;

		List<String> list = null;
		String[] word = line.split("error:\\B");

		
		list = new ArrayList<String>();

		for (i = 1; i <= count; i++) {
			String pattern = null;
			if (word[i].contains("value")) {
				pattern = "(?<=value:\\s).*";
			} 
			else if (word[i].contains("found")) {
				pattern = "(?<=found:\\s).*";
			}
			else if (word[i].contains("numeric instance is lower than the required minimum")) {
				pattern = "(?<=found:\\s).*";
			} 
			
			else if (word[i].contains("numeric instance is greater than the required maximum")) {
				pattern = "(?<=found:\\s).*";
			}
			else if (word[i].contains(
					com.stargate.common.errorHandler.ErrorConstants.unknownBody)) {
				pattern = "(?<=found:\\s).*";
			}
			else {
				pattern = "(?<=expected:\\s).*";
			}
			

			String match = counter.stringMatcher(word[i], pattern);
			if (match != null) {
				list.add(match.replaceAll("[^a-zA-Z0-9-/.,]", ""));
			} else {
				list.add("Unrecognized token");
			}

		}
		return list;
	}

}
