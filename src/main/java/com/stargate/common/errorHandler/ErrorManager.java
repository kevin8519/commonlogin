package com.stargate.common.errorHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class ErrorManager {
	Counter counter= new Counter();

	public List<String> splliiter(String line) {
		
		// to generate the query variable for the database
		int count = 0;
		count = counter.counterFunction(line);
		

		if (count > 0) {
			int i = 1;
			String result = null;
			List<String> list = null;
			String[] word = line.split("error:\\B");

			list = new ArrayList<String>();
			for (i = 1; i <= count; i++) {
				if (((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.Authorization))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.Content_Type))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.gateway_company_id))
						
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.request_id)))
						&& 
								 (word[i].contains("null"))) {

					result = "1400-03-004";
					list.add(result);
				}

				else if (((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.Authorization))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.Content_Type))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.gateway_company_id))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.request_id)))
						&& ((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.shortlen))
								|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.minLength)))) {

					result = "1400-07-003";
					list.add(result);
				}
				else if (((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.Authorization))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.Content_Type))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.gateway_company_id))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.request_id)))
						&& ((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.longlen))
								|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.maxLength)))) {

					result = "1400-07-004";
					list.add(result);
				}
				else if((word[i].contains("(object)"))||(word[i].contains("(array)"))&& (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.allowed_primitive_type))){
					result = "1400-04-001";
					list.add(result);
				}
				
				else if (((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_method))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_id))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_amount))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.debit_credit_indicator))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_description))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_date))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_format))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_name))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_id))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_id_type))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_account_number))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_account_type))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_name))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_account_number))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_account_type))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_id))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_id_type)))
						&& (word[i].contains("null"))) {

					result = "1400-03-005";
					list.add(result);
				}
				
				else if (((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_id))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_description))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_name))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_id))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_account_number))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_name))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_account_number))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_id)))
						&& ((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.shortlen))
								|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.minLength)))) {

					result = "1400-07-003";
					list.add(result);
				}
				else if (((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_id))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_description))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_name))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_id))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_account_number))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_name))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_account_number))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_id)))
						&& ((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.longlen))
								|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.maxLength)))) {

					result = "1400-07-004";
					list.add(result);
				}
				
				else if (((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_id))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_description))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_name))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_id))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_account_number))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_name))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_account_number))
						
						
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_id)))
						&& 
						   (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.allowed_primitive_type))) {

					result = "1400-04-001";
					list.add(result);
				}
				
				else if (((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_method))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.debit_credit_indicator))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_format))
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_name))
					
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_id_type))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payer_bank_information_bank_account_type))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_account_type))
						
						|| (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payee_bank_information_bank_id_type)))) {

					result = "1400-05-003";
					list.add(result);
				}
				
				
				
				else if ((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_amount)) && (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.minimum))){
					result = "1400-07-003";
					list.add(result);
				}
				
				else if ((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_amount)) && (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.maximum))){
					result = "1400-07-004";
					list.add(result);
				}
				else if ((word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_amount)) && (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.allowed_primitive_type))){
					result = "1400-04-001";
					list.add(result);
				}
				else if (word[i].contains(com.stargate.common.errorHandler.ErrorConstants.payment_date)){
					result = "1400-09-004";
					list.add(result);
				}
				else if (word[i].contains(
						com.stargate.common.errorHandler.ErrorConstants.unknownBody)) {
					
					result="1400-03-010";
					list.add(result);
				}
				else {
					result = "1500-01-011";
					list.add(result);
				}
			}

			return list;
		} else {
			String result = null;
			List<String> list = new ArrayList<String>();

			if (line.contains(
					com.stargate.common.errorHandler.ErrorConstants.unknownHeader)) {
				result = "1400-03-009";
				list.add(result);
			}
			else if(line.contains(com.stargate.common.errorHandler.ErrorConstants.jsonParserError)){
				
				result="1400-01-002";
				
				list.add(result);
			}
			else if(line.contains(com.stargate.common.errorHandler.ErrorConstants.jsonParserError1)){
				
				result="1400-01-002";
				
				list.add(result);
			}

			else if (line.contains("Timeout exceeded")) {
				result = "1504-01-003";
				list.add(result);
			}

			else if (line.contains("Method Not Supported")) {
				result = "1405-01-001";
				list.add(result);
			}

			
            else {
				result = "1500-01-011";
				list.add(result);
			}

			return list;
		}

	}

}
